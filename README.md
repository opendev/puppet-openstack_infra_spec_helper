Puppet-OpenStack-Infra Spec Helper
==================================

This gem provides transitive dependencies for the OpenStack Puppet Modules
[OpenStack Infra Modules](http://docs.openstack.org/infra/system-config/sysadmin.html).

Usage
-----

Include this gem in your Gemfile:

```
gem 'puppet-openstack_infra_spec_helper',
  :git => 'https://opendev.org/opendev/puppet-openstack_infra_spec_helper',
  :require => false
```
